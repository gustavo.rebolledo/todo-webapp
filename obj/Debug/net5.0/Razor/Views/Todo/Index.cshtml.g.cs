#pragma checksum "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "bd81e8fb05c1db75346ca6e17f4b8f700067dbc9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Todo_Index), @"mvc.1.0.view", @"/Views/Todo/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\_ViewImports.cshtml"
using Todo_WebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\_ViewImports.cshtml"
using Todo_WebApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bd81e8fb05c1db75346ca6e17f4b8f700067dbc9", @"/Views/Todo/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"049cb38d4dd9c21c211230e113e44140baf30163", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Todo_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Todo_WebApp.Models.ViewModels.TodoViewModel>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
  
    ViewData["Title"] = "Maneja tus listas de tareas";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<div class=\"card\">\r\n    <h5 class=\"card-header\"> ");
#nullable restore
#line 8 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
                        Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@" </h5>
    <div class=""card-body"">
        <table class=""table table-hover"">
            <thead>
                <tr>
                    <th> Estado </th>
                    <th> Tarea </th>
                    <th> Fecha </th>
                </tr>
            </thead>
");
#nullable restore
#line 18 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
             foreach(var item in Model.Items)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("               <tr>\r\n                   <td>\r\n                       <input type=\"checkbox\">\r\n                   </td>\r\n                   <td>\r\n                       ");
#nullable restore
#line 25 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
                  Write(item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                   </td>\r\n                   <td>\r\n                       ");
#nullable restore
#line 28 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
                  Write(item.DueAt);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                   </td>\r\n               </tr> \r\n");
#nullable restore
#line 31 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </table>\r\n        <div>\r\n            \r\n        </div>\r\n    </div>\r\n    <div class=\"card-footer\">\r\n        ");
#nullable restore
#line 38 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 1\Todo WebApp\Views\Todo\Index.cshtml"
   Write(await Html.PartialAsync("AddItemPartial", new TodoItem()));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Todo_WebApp.Models.ViewModels.TodoViewModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
