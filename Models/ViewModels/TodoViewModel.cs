﻿using System.Collections.Generic;

namespace Todo_WebApp.Models.ViewModels
{
    public class TodoViewModel
    {
        public List<TodoItem> Items { get; set; }
    }
}
