﻿using System;
namespace Todo_WebApp.Models
{
    public class TodoItem
    {
        // 4 propiedades 
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }
        public DateTime? DueAt { get; set; }

    }
}
