﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;

using Todo_WebApp.Models;
using Todo_WebApp.Models.ViewModels;

namespace Todo_WebApp.Controllers
{
    public class TodoController : Controller
    {
        //Mockup data
        private static List<TodoItem> Items = new List<TodoItem>()
        {
            new TodoItem() { Id = Guid.NewGuid(), Title = "Certamen 1" },
            new TodoItem() { Id = Guid.NewGuid(), Title = "Certamen 2" },
            new TodoItem() { Id = Guid.NewGuid(), Title = "Certamen 3" },
        };

        public IActionResult Index()
        {
            TodoViewModel viewModel = new TodoViewModel();
            viewModel.Items = Items; 
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Save(TodoItem todoItem)
        {
            if(todoItem == null)
            {
                return BadRequest("Parametros invalidos");
            } else
            {
                if (string.IsNullOrEmpty(todoItem.Title))
                {
                    return BadRequest("El titulo no puede estar vacio");
                }
                else
                {
                    todoItem.Id = Guid.NewGuid();
                    Items.Add(todoItem);
                }
            }

            return RedirectToAction("Index");
        }
    }
}
